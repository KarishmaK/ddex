import os
import cv2
import numpy as np
from PIL import Image, ImageEnhance, ImageFilter
import pytesseract
import csv

basepath = os.path.dirname(__file__)

inputpath = os.path.abspath(os.path.join(basepath, "..", "data\\img8.jpg"))


outputpath = os.path.abspath(os.path.join(basepath, "..", "output"))
if not os.path.exists(outputpath):
    os.makedirs(outputpath + "\\data")
    os.mkdir(outputpath + "\\img")


img = cv2.imread(inputpath)
print img.shape
tmp=1
im_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
im_gray = cv2.bilateralFilter(im_gray, 9, 110, 110)
thresh = cv2.threshold(im_gray, 200, 255, cv2.THRESH_BINARY)
cnts, hierarchy = cv2.findContours(thresh[1], cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
target = open(outputpath+"\\data\\output1.txt", 'w')
writer = csv.writer(target)
de = dict()
cnt = 0
dict1 = dict()
for c in cnts:
    area = cv2.contourArea(c)
    if 4000< area< 6000000: # this is to avoid getting smaller bounding box and the entire doc bounding box
        cnt = cnt +1
        x,y,w,h = cv2.boundingRect(c)
        letter = img[y:y+h,x:x+w]
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        cv2.imwrite(outputpath+"\\img\\"+str(tmp)+'.png', letter)
        im=Image.open(outputpath+"\\img\\"+str(tmp)+'.png')
        width,height=im.size
        text = pytesseract.image_to_string(im)
        size=width,height
        size1=width,height
        nc=0
        while(len(text)<2 and nc<=70): #OCR is calculated in loop in case null is returned
           nc=nc+1
           if(nc<=25):
               size=size[0]+45,size[1]+15
               im_resized = im.resize(size, Image.ANTIALIAS)
               text = pytesseract.image_to_string(im_resized)
           elif(nc>25 and nc<=70):
               size1=size1[0]+15,size1[1]
               im_resized = im.resize(size1, Image.ANTIALIAS)
               text = pytesseract.image_to_string(im_resized)
        dict1[str(x)] = text
        if cnt == 4: # sorting of cells based on its x-axis position
            te1 = dict1.items()
            te2 = [(int(x[0]),x[1]) for x in te1]
            te2.sort(key=lambda x: x[0])
            te3 = [x[1] for x in te2]
            writer.writerow(te3)
            dict1 = dict()
            cnt=0
        tmp=tmp+1
cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
print "task ended"
target.close()